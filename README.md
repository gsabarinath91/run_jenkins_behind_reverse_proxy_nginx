# README #

Reverse proxy configuration to access jenkins running on http://localhost:8080 to http://localhost/jenkins

### What is this repository for? ###

*Reverse proxy configuration to access jenkins running on http://localhost:8080 to http://localhost/jenkins

### How do I get set up? ###

* place the nginx.conf under /etc/nginx in centos 7.x running with nginx and reload nginx service.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* g.sabarinath91@gmail.com